FROM php:8.2.19-fpm-bullseye

COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/

RUN apt-get update && apt-get install --no-install-recommends -y \
    apt-transport-https \
    ca-certificates \
    gnupg2 \
    software-properties-common \
    sudo \
    curl \
    nginx \
    supervisor \
    git \
    unzip \
    bc \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

#==================
RUN pecl channel-update pecl.php.net && apt-get update && apt-get install --no-install-recommends -y \
    # Install PHP extensions.
    libicu-dev \
    imagemagick \
    libmagickwand-dev \
    libnss3-dev \
    libssl-dev \
    libzip-dev \
    build-essential \
    libfreetype6-dev \
    libjpeg-dev \
    libpng-dev \
    libwebp-dev \
    libpq-dev \
    libzip-dev \
    libjpeg62-turbo-dev \
    zip \
    libxslt-dev \
    mariadb-client \
    # https://github.com/mlocati/docker-php-extension-installer \
    # https://www.drupal.org/project/drupal/issues/3116611
    && docker-php-ext-configure gd --enable-gd  --with-freetype --with-webp --with-jpeg\
    && docker-php-ext-install intl xsl gd xml zip mysqli pdo_mysql bcmath calendar sockets pcntl opcache exif \
    && pecl install imagick \
    && docker-php-ext-enable imagick \
    && pecl install redis \
    && docker-php-ext-enable redis \
    # Cleanup.
    && docker-php-source delete \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*


RUN ln -sf /dev/stderr /var/log/nginx/error.log
